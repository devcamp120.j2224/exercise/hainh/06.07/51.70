import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        //1. ví dụ về last indexof nó tính chữ sau cùng dc tìm thấy trong length
        String s1 = "this is index of example";
        int index1 = s1.lastIndexOf('s');
        int index2 = s1.lastIndexOf("ex");
        System.out.println(index1);//6 
        System.out.println(index2);//17

        //2. ví dụ về boolean add(int index, Object o): Thêm một phần tử vào vị trí index. 
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        arr.add(3);
        for (Integer i : arr) {
            System.out.print(i + " ");
        }

        //3. ví dụ về boolean remove(Object o): Xoá object o khỏi ArrayList, object o này phải chứa trong ArrayList.

        ArrayList<Integer> arr1 = new ArrayList<Integer>();
        arr1.add(1);
        arr1.add(3);
        for (Integer i : arr1) {
            System.out.println(i + "truoc khi xoa ");
            arr1.remove("3");
            System.out.println(i + "sau khi xoa");
        }

        //4.  boolean remove(int index): Xoá một phần tử tại vị trí index

        ArrayList<Integer> arr2 = new ArrayList<Integer>();
        arr2.add(1);
        arr2.add(2);
        arr2.add(3);
        int remove = arr2.remove(0);
        System.out.println("Phan tu bi xoa: " + remove);
        System.out.println("Cac phan tu con lai: ");
        for (Integer i : arr2) {
            System.out.println(i + " ");
        }

        //5. ví dụ về Object set(int index, Object o): Cập nhật phần tử tại vị trí index. int , string nhét các kiểu dữ liệu vào

        ArrayList<String> arraylist = new ArrayList<String>();
        arraylist.add("j");
        arraylist.add("a");
        arraylist.add("v");
        arraylist.add("antivirus");
        System.out.println("ArrayList before update: "+arraylist);
        
        arraylist.set(0 , "porn");
        
        arraylist.set(1, "movie");
       
        arraylist.set(2, "is great");
        
        System.out.println("ArrayList after Update: "+arraylist);

        //6.  int indexOf(Object o): Lấy vị trí index của object o trong ArrayList.
        ArrayList<Integer> arraylist1 = new ArrayList<Integer>();
        arraylist1.add(0);
        arraylist1.add(1);
        arraylist1.add(2);
        arraylist1.add(3);
        System.out.println("ArrayList before update: "+arraylist1);
       
        arraylist1.set(0, 2);
        arraylist1.set(1, 4);
        arraylist1.set(2, 5);
        arraylist1.set(3, 6);

        //7. Object get(int index): Return object tại vị trí index trong ArrayList.
        int sume = arraylist1.get(3);
        //8.int size(): lấy số lượng phần tử chứa trong ArrayList
        int sum = arraylist1.size();
        //9.  boolean contains(Object o): Kiểm tra phần tử object o có chứa trong ArrayList, nếu có return true, ngược lại false.
        boolean isExist = arraylist1.contains("deft");
        //10. void clear(): Xoá tất cả các phần tử trong ArrayList
        arraylist1.clear();

        System.out.println("ArrayList after Update: "+arraylist1);
        System.out.println("kết quả cua object get index " +  sume);
        System.out.println("kết qua của int size " + sum);
        System.out.println(isExist);
        System.out.println("các phần tử còn lại " + arraylist1);

    }

}
